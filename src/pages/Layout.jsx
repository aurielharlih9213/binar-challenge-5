import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Footer } from "../assets/components/Footer/Footer";
import { MainNavbar } from "../assets/components/Navbar/MainNavbar";
import { Routers } from "../routers/Routers";

export const Layout = () => {
  return (
    <div>
      <BrowserRouter>
        <MainNavbar />
        <Routers />
      </BrowserRouter>
      <Footer />
    </div>
  );
};
